#!/bin/bash

# Check if csvkit is installed
if ! command -v in2csv &> /dev/null; then
  echo "csvkit is not installed. Please install it using 'pip install csvkit'."
  exit 1
fi

# Get the current date
current_date=$(date +"%Y%m%d%H%M%S")

# Define the filename with the current date for both text and Excel files
text_file="output_${current_date}.txt"
excel_file="output_${current_date}.xlsx"

# Run the commands and save their output to the text file
# All commands are custom Commands, each command is customized to get specific data.
{
  showdate
  checkhealth -svc -detail
  showport -sfp -ddm
  showrcopy
  shownode -d
  showpd
} > "$text_file"

echo "Commands executed, and output saved to $text_file"

# Convert the text file to an Excel file
in2csv "$text_file" | csvformat -T > "$excel_file"

echo "Text file converted to Excel file: $excel_file"
